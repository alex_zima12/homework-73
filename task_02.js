const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;
const app = express();
const port = 8001;
const password = 'key';



app.get('/encode/:encode', (req, res) => {
    res.send(Vigenere.Cipher(password).crypt(req.params.encode));
});

app.get('/decode/:decode', (req, res) => {
    res.send(Vigenere.Decipher(password).crypt(req.params.decode));
});

app.listen(port, () => {
    console.log('Server running at http://localhost:'+ port);
});