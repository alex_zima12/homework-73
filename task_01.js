const express = require('express');
const app = express();
const port = 8000;

app.get('/', (req, res) => {
    res.send('Kak dela??????');
});

app.get('/:answer', (req, res) => {
    res.send('Moi dela' + req.params.answer);
});

app.listen(port, () => {
    console.log(`Server running at http://localhost:` + port);
});
